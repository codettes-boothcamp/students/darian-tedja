## Embedded Programming
Basics

### Week 3  
  
Theo Boomsma teaches the basics of programming. For that we needed to install the  *Arduino* software. After the installation he explained by drawing a system of how energy works on the whiteboard. We also got familiar with the *Arduino Uno board* and some of the different components on it such as *resistors*, *capacitors*, *LED's*, *H-Bridge*, *Jumper wires*, *Transistors*. The Arduino Uno Board is a progammable *micro-controller*.   
  
![Screenshot](../img/emp/62.jpg)  

    
To get started with *Arduino* the settings needed to be adjust. And I did so by doing the following steps.  
  
![Screenshot](../img/emp/50.jpg)  
  
![Screenshot](../img/emp/51.jpg)  
 
Arduino Blink sketch here below.
 
``` 
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```
  
  
**Spaceship Interface**  
  
For good practice always start your code by setting the frame first.  
  
```
void setup(){
}  
  
void loop(){  
}
```  

Declare the switchState and give it a value.  
Like so ```Int switchState = 0;```  
  
**Running lights** (basic explination of code) 
 
 Here we added a second LED to make the lights run in a loop.  
   
```  
  int switchState = 0;
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(12, 13, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(12, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(12, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second  
  digitalWrite(13, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(500);                       // wait for a second
  digitalWrite(13, LOW);    // turn the LED off by making the voltage LOW
  delay(500);                       // wait for a second
}

```

**Procedural programming**(Optimizing of code)   
  
```
 void setup() {
// setup your hardware
}
void ledsOff() {
//switch all leds off
digitalWrite(3,0);
digitalWrite(4,0);
digitalWrite(5,0);
}
void runningLeds(){
	ledsOff();
	digitalWrite(3,1);
	delay(250);
	ledsOff();
	digitalWrite(4,1);
delay(250);
	ledsOff();
	digitalWrite(5,1);
delay(250);
}  

void loop() {
	switchState = digitalRead(2);
	if(switchState==0){
	ledsOff();
	digitalWrite(3,HIGH);
}else{
	runningLeds();
}
}
```
  
= assignment
== comparison
!= comparison

**Serial communication**  
  
```
void setup() {
  Serial.begin(9600);
  Serial.println("hello world");

}

void loop() {
  Serial.println(millis());
  delay(2000);

}
```  

**LDR**  
  
```
int lightInt=0;
  int minL=700;
  int maxL=1023;
  
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
   pinMode(2, INPUT);
   pinMode(3, OUTPUT);
   pinMode(4, OUTPUT);
   pinMode(5, OUTPUT);
  
}

void ledsOff() {
//switch all leds off
//digitalWrite(2,0);
  digitalWrite(3,0);
  digitalWrite(4,0);
  digitalWrite(5,0);
}

void runningLeds(){
  ledsOff();
  digitalWrite(3,1);
  delay(250);
  ledsOff();
  digitalWrite(4,1);
  delay(250);
  ledsOff();
  digitalWrite(5,1);
  delay(250);
}

void dimLed(int ledPin,int dutyCycle){
//cycle time =100ms
//led aan
//switch all leds off
//digitalWrite(2,0);
  digitalWrite(3,0);
  digitalWrite(4,0);
  digitalWrite(5,1);
  delay(
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  // int sensorValue = analogRead(A0);
  // print out the value you read:
  lightInt= analogRead(A0);
  int lightPc=(lightInt-minL)*100L/(maxL-minL);
  Serial.println(lightPc);
  ledsOff();
  // if(lightPc>=30){digitalWrite(2,1);}
  if(lightPc>=30){digitalWrite(3,1);}
  if(lightPc>=60){digitalWrite(4,1);}
  if(lightPc>=90){digitalWrite(5,1);}
  if(lightPc>95){runningLeds();};
  delay(1000);
}
  
  Serial.println("Team Up");
  delay(250);        // delay in between reads for stability
```  
   

**PWM**  
  
```
  int lightInt=0;
  int minL=700;
  int maxL=1023;
  
// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
   pinMode(2, INPUT);
   pinMode(3, OUTPUT);
   pinMode(4, OUTPUT);
   pinMode(5, OUTPUT);
  
}

void ledsOff() {
//switch all leds off
//digitalWrite(2,0);
  digitalWrite(3,0);
  digitalWrite(4,0);
  digitalWrite(5,0);
}

void runningLeds(){
  ledsOff();
  digitalWrite(3,1);
  delay(250);
  ledsOff();
  digitalWrite(4,1);
  delay(250);
  ledsOff();
  digitalWrite(5,1);
  delay(250);
}

void dimLed(int ledPin,int dutyCycle){
  int cycleTime =10;
//led aan
//switch all leds off
  digitalWrite(3,1);
  digitalWrite(5,1);
  delay(dutyCycle*cycleTime/100);
  digitalWrite(5,0);
  delay(cycleTime-dutyCycle*cycleTime/100);
  
}


void loop() {
 
  lightInt= analogRead(A0);
  int lightPc=(lightInt-minL)*100L/(maxL-minL);
  Serial.println(lightPc);
  dimLed(5,10);  
  analogWrite(3,127.5); //(analogwrite heeft als formule aantal percentage *255/100) 50*255/100=127.5
} 
```  
  
***Temp humidity***  
  
```
	#include "dht.h"
	#define dht_apin A0 // Analog Pin sensor is connected to
 
	dht DHT;
 
void setup(){
 
  Serial.begin(9600);
  delay(500);//Delay to let system boot
  Serial.println("DHT11 Humidity & temperature Sensor\n\n");
  delay(1000);//Wait before accessing Sensor
 
}//end "setup()"
 
void loop(){
  //Start of Program 
 
    DHT.read11(dht_apin);
    
    Serial.print("Current humidity = ");
    Serial.print(DHT.humidity);
    Serial.print("%  ");
    Serial.print("temperature = ");
    Serial.print(DHT.temperature); 
    Serial.println("C  ");
    
    delay(5000);//Wait 5 seconds before accessing sensor again.
 
  //Fastest should be once every two seconds.
 
}// end loop() 
```  
  
### Working in tinkercad online simulation  
  
![Screenshot](../img/emp/68.jpg)
	
**Motor control H-bridge tinkercad**    
  
H-Bridge - DC Motor 

```
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}

void loop() {
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,255) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}

```  
  
H-Bridge - DC Motor - Adding the speed control  

```  
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
// Speed control
Const int speedPin = A0;
Int speed =0;

//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}

void loop() {
   // Detect speedPin value
   speed=analogRead(speedPin)/4;
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,speed) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}

```  
   
**H-Bridge - DC Motor - speed & direction control**
  
![Screenshot](../img/emp/71.jpg)
  
```
// Declare ur variables
const int pwm = 3;
const int in_4 = 8;
const int in_3 = 9;


void setup(){
	pinMode(in_3, OUTPUT);	
  	pinMode(in_4, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
} 

void loop(){
  	int duty = (analogRead(A0)-512)/2;
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty));
  	if(duty>0){
        // turn CW
      digitalWrite(in_3,LOW);
      digitalWrite(in_4,HIGH); 
    }
  	if(duty<0){
        // turn CCW
      digitalWrite(in_3,HIGH);
      digitalWrite(in_4,LOW); 
    }
  	if(duty==0){
        // BRAKE
      digitalWrite(in_3,HIGH);
      digitalWrite(in_4,HIGH); 
    }  
}
```   
   
**H-Bridge - DC Motor - speed & direction control(using Mapping function)**  

A potentiometer is a type of voltage divider. As I turn the knob, I change the ratio of the voltage between the middle pin and power.
So I can read this change on an analog input. I have connect the middle pin to the analog pin 0.
This will control the position of my Servo motor.
The map function is intended to change one range of values into another range of values and a common use is to read an analogue input (10 bits long, so values range from 0 to 1023)
and change the output to a byte so the output would be from 0 to 255.  
 
 
**Servo Motor**
The three wires of the servo motor are of different use. One is for power, one is for ground, and the third is the control line that will receive information from the Arduino.
There are also different voltage servo motors. The servo motor is connected to an analog port on your arduino board and controlled by the PWM which gives a certain voltage.  
  
![Screenshot](../img/emp/72.jpg)
  
```
#include <Servo.h>
Servo myServo;
int const potPin = A0;
int potVal;
int angle;
void setup(){
  myServo.attach(9);
  Serial.begin(9600);
} 

void loop(){
  potVal=analogRead(potPin);
  Serial.print("potVal:");
  Serial.print (potVal);
  angle=map(potVal,0,1023,0,179);
  Serial.print(",angle:");
  Serial.println(angle);
  //analogWrite(9, map(potVal,0,1023,0,255));
  myServo.write(angle);
  delay(15);
}
```

  