## Interface  

### Week 6 
**Raspberry Pi 2**  
  
The Raspberry Pi is a microcomputer which is used for educational purposes but can also be used for hobbyist.  
  
![Screenshot](../img/.jpg)  
  
Curious about Raspberry Pi 2 specs? Here is a quick list of Raspberry Pi 2 features:

CPU: Broadcom BCM2836 900MHz quad-core ARM Cortex-A7 processor
RAM: 1 GB SDRAM
USB Ports: 4 USB 2.0 ports  (same as Raspberry Pi B+)
Network: 10/100 Mbit/s Ethernet (same as Raspberry Pi B+)
Power Ratings: 600 mA (3.0 W) (same as Raspberry Pi B+)
Power Source: 5V Micro USB (same as previous models)
Size: 85.60 mm × 56.5 mm (same as Raspberry Pi B+)
Weight: 45 g (same as Raspberry Pi B+)
Price: $35 (same as Raspberry Pi B+)  
  
    
In order to configure the Raspberry Pi we needed a SD card and a so called "picture" Raspbian Jessie which we had to write on the SD card by using Win32Diskmanager.  
  
![Screenshot](../img/.jpg)  
  
  
![Screenshot](../img/.jpg)  
  
  
**Note**  
After configuring the SD Card, you will find a Boot-drive on your PC. Open it, then open cmdline.txt in notepad ++ and then hardcode your adress on your Raspberry Pi. My Raspberry Pi adress for example is 192.168.1.157
This is needed to install or work with putty. Go to your SD-card drive and make a new text file, name it SSh, without the text extention to allow putty to access the Raspverry Pi remotely.  
  
  
 I connected my Raspberry Pi to my laptop using a ethernet crossover cable. An Ethernet crossover cable is a crossover cable for Ethernet used to connect computing devices together directly.  
   
   
![Screenshot](../img/.jpg)  
  
![Screenshot](../img/.jpg)  