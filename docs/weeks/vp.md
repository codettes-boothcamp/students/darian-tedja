## Video production  

### Week 2  
**Premiere Pro**  
In this week we were being taught the basics of premiere pro. For those who didn't had the program needed to install it first. In my case I was already familiar with this program and had a few of the adobe softwares but I did learn about a shortcut that I didn't knew. So that did came in handy.  

**Step 1**  
Make a new project, name it and choose where you want to save your project.  

![Screenshot](../img/18.jpg)  
     
![Screenshot](../img/19.jpg)  
  
After clicking OK my workspace looks like this.  
     
![Screenshot](../img/20.jpg)    

**Step 2**  
Import your files. There are 3 ways to do that.  
-Drag & Drop  
-File & then import   
-CTRL + I  

![Screenshot](../img/21.jpg) 
    
![Screenshot](../img/33.jpg)  
 
**Step 3**  
Place footage on timeline. There are different ways to do this also.  
-Drag & Drop to timeline.  
-Drag to *New Item*. By choosing this option the timeline will take the exact settings of the footage in which it is shot.  
  
![Screenshot](../img/39.jpg) 
    
![Screenshot](../img/40.jpg)

  
**Step 4**  
Making sure to remove the *GreenScreen* before cutting my footage.   
  
I removed my greenscreen with a preset I already made beforehand which makes it easier for me to edit and is less time consuming. But the basics steps will be below in the screenshots.
  
![Screenshot](../img/vp/44.jpg)  
  
  Drag the *Ultra Key* onto footage  
![Screenshot](../img/vp/45.jpg)  
  
  
**Step 5**  
*Cutting* footage and adding *titles or text*. You can follow the screenshots below to see how it's done.   
  
You can activate the cutting tool by clicking on the *razor tool* or by pressing "C" on your keyboard and cut the footage on the timeline where ever it needs to be cut.
  
![Screenshot](../img/vp/37.jpg)  
  
![Screenshot](../img/vp/38.jpg)  
  
  
I add titles to my video by doing the following steps.  

 ![Screenshot](../img/vp/49.jpg)  
  
  
![Screenshot](../img/vp/46.jpg)  
  
  
![Screenshot](../img/vp/47.jpg)  
  
  
![Screenshot](../img/vp/48.jpg)    
  
**Step 6**  
  
*Exporting* footage for upload. We needed to include an in-/outpoint before exporting to make sure everything that needed to be exported would be exported.    
  
  
  
![Screenshot](../img/vp/42.jpg) 
  
![Screenshot](../img/vp/41.jpg)  
  
  Make sure the format is on H.264 and set preset to what's suitable for your pc or laptop. Also make sure to name your video in the output tab. After doing that I hit export.  
    
![Screenshot](../img/vp/43.jpg)  