## Project Managment
Project Managment

### Week 1 

**Signing up**  
The first thing we had to do was signing up and clone **GitLab** and **Github Desktop**  

**GitLab**    

![Screenshot](../img/3.jpg) 

![Screenshot](../img/11.jpg)        

**Installing softwares**  
The first softwares I had to install were the GitHubDesktop and notepad+ (I already had notepad+). These are the softwares to get access to work group related or solo.    

![Screenshot](../img/10.jpg) 

**Github Desktop**   

![Screenshot](../img/34.jpg)    

**Local file system**  

![Screenshot](../img/12.jpg)   

**Structured documentation**  
I structured out my documentation starting off with a picture of myself followed by a short bio in my notepad+ editor. 
I then began to do my basic outline for my documentation. After testing and running in to minor errors I find it easier to work local,
and then synchronize my github desktop with the online files. That's worked out good so far. 

![Screenshot](../img/pm/13.jpg)   

**Github Desktop change dettection and committing text**

![Screenshot](../img/pm/15.jpg)   

**Git commit**

![Screenshot](../img/pm/16.jpg)   

**Git push**

![Screenshot](../img/pm/17.jpg)   

**GitLab Change made**  
Before I editing in notepad++,committing and pushing.  

![Screenshot](../img/pm/14.jpg)   

After editing in notepad++, committing and pushing
 
![Screenshot](../img/.jpg) 



