##Electronic Desgin  
  
###Week 
This week we learned to design atmega328p microcontroller board using KiCad And Flatcam

To do
Connect Components
Give Components correct name and value
Annotate the design
Assign Footprint to Symbols
Generate Netlist
Open Pcb New
Read in Generate netlist
In Pcb New Set Design Rules (Setup -Design Rules)
Rearrange Components and Make Traces
Place your Auxiliary Axis
Plot Layers

**First I had to open a new project in KICAD**

New-->Project
Preferences-->Manage Symbol Libraries

**Then you Import the Fab.lib**


**After importing the Fab.lib I had to select the components from the components list:**
-Atmega328p-Au
-2x Resistor
-1x 1uF Capacitor
-1x 0.1uF Capacitor
-Led
-Switch
-Resinator
-3x Header




***Make sure to annotate to check if the tags are labeled correctly***


I selected assign footprint to pcb and added my footprint.

FootPrintLib-->Fab.pretty
Fab.pretty-->Fab.mod



Make schematic in KiCad






**Select Generate netlist and import netlist to Pcb new**
  
![Screenshot](../img/ed/3.jpg)




**KiCad Organize your components. Use Route tracks , Auxiliary Axis and dimension**



KiCad Plot

FrontCut
Output Directory
Aux As Orgin



After plotting and generating my gerber files I then opened Flatcam to add parameters



File --> Open Gerber



Go to you Gerber options and select mm




Select Gerber

Isolation Routing

Tool dia - 0.2
width (passes) 1
Pass overlay 0.170000

Board Cutout

Tool dia - 0.8
Margin: 1.5
Gap size: 0.5


For the Front Cut select

Cut Z  -0.12
Travel Z 2.0
Feedrate  30mm/min
Tool dia 0.2
Spindle-speed 1000


Then Export Gcode
For the Edge Cut select

Cut Z  -1.5
Travel Z 0.1
Feedrate  20 mm/min
Tool dia 0.8
Spindle-speed 1000
Multi Depth (check)
Depth pass 0.5

