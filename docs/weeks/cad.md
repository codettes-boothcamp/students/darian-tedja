## 3D Design and 3D Printing  
  
**Assignment:**

Practicing with the 3D Designs in TinkerCad
Installing Fusion 360
Installing Inkscape
Printing the design with the 3D printer: AnyCubic for Maxpro

**Learning outcomes:**

1. Making 3D Design with all kinds of shapes
2. Working with X, Y and Z-axis
3. Putting the settings for the AnyCubic For Maxpro 3D Printer

**Assessment:**
Have you:

1. Design your own 3D Project in TinkerCad or Fusion 360
2. Design your own 3D Project in Fusion 360
3. Print your own 3D Design with the AnyCubic for Maxpro

3D Designing in TinkerCad
In the Christmas break we got the time to test and play with TinkerCad making a 3D Design to print for the first time.
I have to say that working with the Z-as was very difficult, especially the first time. Choosing the ojects wasn't the problem but blending them in one was the real issue.
But then again the snowman is standing...  
  
  
**Fusion 360**  
After downloading Fusion 360 we got to know the program by checking out all the shortcuts for drawing your own 3D Design.
it didn't differ to much  from the tinkercad but you got more options in Fusion 360. The environment was more specific, more options.
C: Circles
R: Rectangles
L: a Line
E: Extrude
H: Hole
M: Move  
  
  
There are a lot of things (images, pictures) which we can use in Fusion 360, but we have to get the right files (extentions).
These files can be a:

1. obj file
2. stl file (Stereo lithography)
3. png (for this one we may edit it first in inkscape)

There are 3 most important steps in Fusion

1. Scatch
2. Body
3. Modify

Working with InkScape
We need svg-files for the Fusion project and we are editing it in inkscape using:
Shift+Alt+B, looking for the Bitmap file by clicking the Path: Trace bitmap.
check the box: for Live preview and save the file as a SVG file.
After doing this import the SVG file in Fusion.
Note: always place the object in the center of the axis. Because it's easy to find your work in the workpane.
  

