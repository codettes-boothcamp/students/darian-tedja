# Project Management

![Screenshot](img/legendari.png)
	

# Write an about
My name is Darian Tedja. I am a young woman who enjoys and loves good music and technology. 
At a very young age I got to learn the basics of how to work with a computer. 
At this point I was living abroad. 
After  5 years of living abroad I came back to my country and working with the computer was not really optional until I graduated to highschool. 
In highschool I was stoked to know that computer lessons were on our schedule. I soon got bored cause I had already learned about the basics abroad. 
So I turned back to music. The following years I studied music, 1 year at CvS  and 5 years at IOL, which didn’t work out either. 
You could say that I lost valuable time. I then got a scholarship at Codette’s Girls innovative Bootcamp. Here is where my love for ICT came back. 
And the journey starts right here…… 

## Research final project
1. **Gimbal Stabilizer**

![Screenshot](img/Ronin.png)  ![Screenshot](img/Brushless.png)

I'm into the creative content world. I even followed a training at [Studio Paramaribo](https://www.facebook.com/StudioParamaribo/) for E.N.G where we had to learn how to control a camera and more. I found it hard to film handheld and came across different stabilizers. This inspired me to start making my own camera gimbal stabilizer. I first needed to understand what axis are and came across this useful website where they explained the different axis.
[click here](https://www.evogimbals.com/blogs/evo-blog/how-does-a-3-axis-gopro-or-dslr-gimbal-work)  
You can also take a look at this tutorial to get a better understanding of a 3-axis gimbal stabilizer [click here](https://www.youtube.com/watch?v=K5PByCrXMlk)  

![Screenshot](img/Sketch.png)

2. **Ice maker sealing machine**  

![Screenshot](img/Ice.png)

The idea of making a self-sealing ice-maker machine comes from my mom. She makes the most delicious homemade popsicles. The only thing that turns me off or is as I say it time consuming. I want my mom to be able to make the mixture only and let this machine take care of the rest so she can spent more time relaxing.


## Objectives
-[x] Writing down an about of yourself.  
-[x] Do research on your final project.  
-[x] Basic Documentation outline.  
-[x] Document in pm.md project management (what was done so far)  



Footnote	Here's a sentence with a footnote. [^1]

[^1]: This is the footnote.

Heading ID	### My Great Heading {#custom-id}

Definition List	term
: definition








